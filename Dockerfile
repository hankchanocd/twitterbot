FROM python:3.7-alpine

COPY TwitterBot/config.py /TwitterBot/
COPY TwitterBot/autoreply.py /TwitterBot/
COPY requirements.txt /tmp
RUN pip3 install -r /tmp/requirements.txt

WORKDIR /TwitterBot
CMD ["python3", "autoreply.py"]
