import tweepy
import logging
from config import create_api
import time

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

def check_mentions(api, keywords, since_id):
    logger.info("Retrieving mentions")
    new_since_id = since_id
    for tweet in tweepy.Cursor(api.mentions_timeline, since_id=since_id).items():
        new_since_id = max(tweet.id, new_since_id)

        # Skip if it's a reply tweet
        if tweet.in_reply_to_status_id is not None:
            continue

        # If matches a keyword
        if any(keyword in tweet.text.lower() for keyword in keywords):
            answer_text = "Please reach us via DM"
            logger.info(f"Answering to {tweet.user.name}'s tweet: {tweet.text}")
            logger.info(f"Answered text is '{answer_text}'\n")
            api.update_status(status=answer_text, in_reply_to_status_id=tweet.id,)

    return new_since_id

def main():
    api = create_api()
    since_id = 1
    keywords = ["help", "support", "advice", "advise", "suggestion", "idea", "ideas"]
    print(f"The keywords searched for in the mentioned tweets are {keywords}")

    # Execute it every 60 sec
    while True:
        print(f"The since_id is {since_id}")
        try:
            since_id = check_mentions(api=api, keywords=keywords, since_id=since_id)
        except:
            print("Connection problem to Twitter API...")
        logger.info("Waiting... ")
        time.sleep(60)

if __name__ == "__main__":
    main()
